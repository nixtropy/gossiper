# Gossiper

<p style="text-align: justify"> Se encuentra enfocado en el aspecto pragmático de la comunicación, en la intención comunicativa, especialmente en las vocalizaciones, fonemas, onomatopeyas y palabras iniciales que debe dar un niño. 
</p>

<p style="text-align: justify">Gossiper busca enseñar primero los fonemas, que son los sonidos mínimos que debe decir el niño para pronunciar las palabras, a partir de juegos con colores, imagine por un momento una esfera de luz o un oso de peluche cuya barriga se ilumine con múltiples colores, se coloca en el centro de la habitación y los padres o el terapeuta le hablan al juguete, por ejemplo hacen el sonido asociado al fonema /r/, como un gruñido, y se enciende de color rojo con una animación vistosa, /as/ y la panza se enciende en un color azul, /xa/ y se enciende de color naranja. A medida que continúen el juego el niño se verá interesado en participar, si la situación es favorable incluso se podrían dejar de lado los fonemas e introducir palabras completas.
</p>


# Diseño de PCB
[Link a EasyEDA](https://easyeda.com/editor#id=|aa1bc3ff85fd443ca3bed44611081820) 

