#ifndef GOSSIPER_H
#define GOSSIPER_H

#include "Arduino.h"
#include "VoiceRecognitionV3.h"

// Grupo Colores
#define azul     (0)
#define rojo     (1)
#define verde    (2)
#define amarillo (3)
#define morado   (4)
#define naranja  (5)
#define rosado   (6)

//Grupo ropa
#define pantalon  (7)
#define falda     (8)
#define zapatos   (9)
#define camisa    (10)
#define saco      (11)
#define tennis    (12)
#define maleta    (13)


class Gossiper {
 public:
 Gossiper(HardwareSerial &writer, VR &escuchar);
 void test();
 void cargarColores();
 void cargarRopa();
 private:
 HardwareSerial* printer;
 VR* escuchando;
};


#endif


