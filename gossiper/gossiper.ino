/**
  ******************************************************************************
    @file    vr_sample_control_led.ino
    @author  JiapengLi
    @brief   This file provides a demostration on
              how to control led by using VoiceRecognitionModule
  ******************************************************************************
    @note:
        voice control led
  ******************************************************************************
    @section  HISTORY

    2013/06/13    Initial version.
*/

#include <SoftwareSerial.h>
#include "VoiceRecognitionV3.h"
#include "gossiper.h"
#include <Adafruit_NeoPixel.h>

/**
  Connection
  Arduino    VoiceRecognitionModule
   10   ------->     TX
   11   ------->     RX
*/
VR myVR(10, 11);   // 2:RX 3:TX, you can choose your favourite pins.

uint8_t records[7]; // save record
uint8_t buf[64];
//======================================================================
//informacion asociada al neopixel

#define PIXEL_PIN    6    // Digital IO pin connected to the NeoPixels.
#define PIXEL_COUNT 12
// Parameter 1 = number of pixels in strip,  neopixel stick has 8
// Parameter 2 = pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_RGB     Pixels are wired for RGB bitstream
//   NEO_GRB     Pixels are wired for GRB bitstream, correct for neopixel stick
//   NEO_KHZ400  400 KHz bitstream (e.g. FLORA pixels)
//   NEO_KHZ800  800 KHz bitstream (e.g. High Density LED strip), correct for neopixel stick
Adafruit_NeoPixel strip = Adafruit_NeoPixel(PIXEL_COUNT, PIXEL_PIN, NEO_GRB + NEO_KHZ800);

#define delayturnon 50

//======================================================================
//pines de interrupcion
const byte buttonJuego = 2;
volatile byte grupo = 1;
//======================================================================
/**
  @brief   Print signature, if the character is invisible,
           print hexible value instead.
  @param   buf     --> command length
           len     --> number of parameters
*/
void printSignature(uint8_t *buf, int len)
{
  int i;
  for (i = 0; i < len; i++) {
    if (buf[i] > 0x19 && buf[i] < 0x7F) {
      Serial.write(buf[i]);
    }
    else {
      Serial.print("[");
      Serial.print(buf[i], HEX);
      Serial.print("]");
    }
  }
}

/**
  @brief   Print signature, if the character is invisible,
           print hexible value instead.
  @param   buf  -->  VR module return value when voice is recognized.
             buf[0]  -->  Group mode(FF: None Group, 0x8n: User, 0x0n:System
             buf[1]  -->  number of record which is recognized.
             buf[2]  -->  Recognizer index(position) value of the recognized record.
             buf[3]  -->  Signature length
             buf[4]~buf[n] --> Signature
*/
void printVR(uint8_t *buf)
{
  Serial.println("VR Index\tGroup\tRecordNum\tSignature");

  Serial.print(buf[2], DEC);
  Serial.print("\t\t");

  if (buf[0] == 0xFF) {
    Serial.print("NONE");
  }
  else if (buf[0] & 0x80) {
    Serial.print("UG ");
    Serial.print(buf[0] & (~0x80), DEC);
  }
  else {
    Serial.print("SG ");
    Serial.print(buf[0], DEC);
  }
  Serial.print("\t");

  Serial.print(buf[1], DEC);
  Serial.print("\t\t");
  if (buf[3] > 0) {
    printSignature(buf + 4, buf[3]);
  }
  else {
    Serial.print("NONE");
  }
  Serial.println("\r\n");
}

Gossiper gossip(Serial, myVR);

void setup()
{ 
  /** configurar pin de interrupcion */
  pinMode(buttonJuego, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(buttonJuego), seleccionGrupo, FALLING);
  /** clear strip */
  strip.begin();
  strip.clear(); 
  strip.show();
  /** initialize voice recognition */
  myVR.begin(9600);

  Serial.begin(115200);
  Serial.println("Hola me llamo gossiper quieres jugar?");

  if (myVR.clear() == 0) {
    Serial.println("Humm... creo que olvide lo que aprendi.");
  } else {
    Serial.println("No puedo oirte, no encuentro mis oidos.");
    Serial.println("Por favor revisa la conexion o reiniciame.");
    while (1);
  }
  
  Serial.println("Ahora puedo recordar los colores.");
  gossip.cargarColores();

}

void loop()
{
  int ret;
  ret = myVR.recognize(buf, 50);
  if(ret>0){
    switch(buf[1]){
      //----------------------------------------------------------
      case azul:
            for (int i=0;i<PIXEL_COUNT;i++){
              strip.setPixelColor(i,strip.Color(0,0,100)); // azul
              strip.show();
              delay(delayturnon);
            }
            delay(2000);
            strip.clear();
            strip.show();
      break;
      //----------------------------------------------------------
      case rojo:
            for (int i=0;i<PIXEL_COUNT;i++){
              strip.setPixelColor(i,strip.Color(100,0,0)); // rojo
              strip.show();
              delay(delayturnon);
            }
            delay(2000);
            strip.clear();
            strip.show();
      break;
      //----------------------------------------------------------
      case verde:
            for (int i=0;i<PIXEL_COUNT;i++){
              strip.setPixelColor(i,strip.Color(0,100,0)); // verde
              strip.show();
              delay(delayturnon);
            }
            delay(2000);
            strip.clear();
            strip.show();
      break;
      //----------------------------------------------------------
      case amarillo:
            for (int i=0;i<PIXEL_COUNT;i++){
              strip.setPixelColor(i,strip.Color(189,183,0)); // amarillo
              strip.show();
              delay(delayturnon);
            }
            delay(2000);
            strip.clear();
            strip.show();   
      break;
      //----------------------------------------------------------
      case morado:
            for (int i=0;i<PIXEL_COUNT;i++){
              strip.setPixelColor(i,strip.Color(101,0,189)); // morado
              strip.show();
              delay(delayturnon);
            }
            delay(2000);
            strip.clear();
            strip.show();     
      break;
      //----------------------------------------------------------
      case naranja:
             for (int i=0;i<PIXEL_COUNT;i++){
              strip.setPixelColor(i,strip.Color(150,50,0)); // Naranja
              strip.show();
              delay(20);
            }
            delay(2000);
            strip.clear();
            strip.show();     
      break;
      //----------------------------------------------------------
      case rosado:
            for (int i=0;i<PIXEL_COUNT;i++){
              strip.setPixelColor(i,strip.Color(255,0,247)); // rosado
              strip.show();
              delay(delayturnon);
            }
            delay(2000);
            strip.clear();
            strip.show();     
      break;
      //----------------------------------------------------------      
      default:   
            strip.clear();
            strip.show();           
      break;
    }
    /** voice recognized */
    printVR(buf);
  }
}

//-----------------------------------
// Funciones interrupcion
void indicadorSeleccion(byte grupo) {
    strip.clear(); 
    strip.show();
    
    switch (grupo) {
      case 1:  // 
        Serial.println("modo 1");
        indicadorVisual(grupo);
        myVR.clear();
        gossip.cargarColores();        
      break;
      case 2:  // 
        Serial.println("modo 2");
        indicadorVisual(grupo); 
        myVR.clear();
        gossip.cargarRopa();
      break;
      case 3:  // 
        Serial.println("modo 3");
        indicadorVisual(grupo);
      break;
      case 4:  // 
        Serial.println("modo 4");
        indicadorVisual(grupo);
      break;
      case 5:  // 
        Serial.println("modo 5");
        indicadorVisual(grupo);
      break;
      case 6:  // 
        Serial.println("modo 6");
        indicadorVisual(grupo);
      break;
      case 7:  // 
        Serial.println("modo 7");
        indicadorVisual(grupo);
      break;
      default:
        Serial.println("default modo 1");
      break;
    }
}

//-----------------------------------
// funcion de interrupcion del pin 2

void indicadorVisual(byte grupo){
    strip.setPixelColor(8,strip.Color(0,50,0)); // rojo
    strip.setPixelColor(10,strip.Color(0,80,0)); // rojo
    strip.show();
    
    for (int i=0;i<grupo;i++){
     strip.setPixelColor(i,strip.Color(50,0,0)); // rojo
     strip.show();
     delay(20);
    }

    delay(2000);
    strip.clear();
    strip.show();
  }

void seleccionGrupo() {
  grupo++;
  if(grupo == 8)
    grupo = 1;
    indicadorSeleccion(grupo);  
} 


