#include "Arduino.h"
#include "gossiper.h"
#include "VoiceRecognitionV3.h"

Gossiper::Gossiper(HardwareSerial &writer, VR &escuchar){
  printer = &writer;
  escuchando = &escuchar;
}

void Gossiper::test() {
  printer->println("Hola Nicolas, puedo verte");
}

void Gossiper::cargarColores() {
  printer->println("Vamos a jugar con colores intenta decirlos");
  
  if (escuchando->load((uint8_t)azul) >= 0) {
    printer->println("azul");
  }

  if (escuchando->load((uint8_t)rojo) >= 0) {
    printer->println("rojo");
  }

  if (escuchando->load((uint8_t)verde) >= 0) {
    printer->println("verde");
  }

  if (escuchando->load((uint8_t)amarillo) >= 0) {
    printer->println("amarillo");
  }

  if (escuchando->load((uint8_t)morado) >= 0) {
    printer->println("morado");
  }

  if (escuchando->load((uint8_t)naranja) >= 0) {
    printer->println("naranja");
  }

  if (escuchando->load((uint8_t)rosado) >= 0) {
    printer->println("rosado");
  }
}

void Gossiper::cargarRopa() {
  printer->println("Vamos a jugar con prendas de vertir, intenta decirlas");
  
  if (escuchando->load((uint8_t)pantalon) >= 0) {
    printer->println("pantalon");
  }

  if (escuchando->load((uint8_t)falda) >= 0) {
    printer->println("falda");
  }

  if (escuchando->load((uint8_t)zapatos) >= 0) {
    printer->println("zapatos");
  }

  if (escuchando->load((uint8_t)camisa) >= 0) {
    printer->println("camisa");
  }

  if (escuchando->load((uint8_t)saco) >= 0) {
    printer->println("saco");
  }

  if (escuchando->load((uint8_t)tennis) >= 0) {
    printer->println("tennis");
  }

  if (escuchando->load((uint8_t)maleta) >= 0) {
    printer->println("maleta");
  }
}

